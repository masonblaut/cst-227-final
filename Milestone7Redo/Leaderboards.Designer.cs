﻿using MinesweeperClasses;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Milestone7Redo
{
    partial class Leaderboards
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        GUIAdapter adapter;
        Button[] btnArray;
        Stats theStats;
        T[] top5;

        public void setAdapter(GUIAdapter theAdapter)
        {
            adapter = theAdapter;
            labelName.Text = adapter.getPlayerName();
            labelScore.Text = adapter.getPlayerScore().ToString();
            labelTime.Text = adapter.getPlayerTime().ToString();
            theStats = new Stats(theAdapter);
            top5 = theStats.Top5Saves().Cast<T>().ToArray();
        }

        public void populateList()
        {
            btnArray = new Button[5];

            for (int i = 0; i < 5; i++)
            {
                btnArray[i] = new Button();

                btnArray[i].Width = panel1.Width;
                btnArray[i].Height = panel1.Height / 5;
                panel1.Controls.Add(btnArray[i]);
                try { btnArray[i].Text = "Player: " + top5[4 - i].Name + "  Score: " + top5[4 - i].Score + "  Time: " + top5[4 - i].Time + " Difficulty: " + top5[4 - i].Difficulty; }
                catch (System.IndexOutOfRangeException) { }
                btnArray[i].Location = new Point(0, (panel1.Height / 5) * i);
            }

        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Font = new System.Drawing.Font("OCR A Extended", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 108);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 318);
            this.panel1.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("OCR A Extended", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(30, 75);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(81, 19);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "label1";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("OCR A Extended", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.Location = new System.Drawing.Point(250, 75);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(81, 19);
            this.labelScore.TabIndex = 2;
            this.labelScore.Text = "label1";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("OCR A Extended", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.Location = new System.Drawing.Point(442, 75);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(81, 19);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(167, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 30);
            this.label1.TabIndex = 4;
            this.label1.Text = "- GAME OVER -";
            // 
            // Leaderboards
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(559, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.panel1);
            this.Name = "Leaderboards";
            this.Text = "Leaderboards";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelTime;
        private Label label1;
    }
}