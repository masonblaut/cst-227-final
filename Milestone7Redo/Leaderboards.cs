﻿using MinesweeperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone7Redo
{
    public partial class Leaderboards : Form
    {
        public Leaderboards()
        {
            InitializeComponent();
        }


        public Leaderboards(GUIAdapter theAdapter)
        {
            InitializeComponent();
            setAdapter(theAdapter);
            populateList();
        }


    }
}
