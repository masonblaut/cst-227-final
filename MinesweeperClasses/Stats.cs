﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace MinesweeperClasses
{
    public class Stats
    {
        //Mason Blaut - This is my own work

        GUIAdapter adapter;
        private String path = @"C:\Users\ROG\source\repos\Milestone7Redo\Milestone7Redo\Resources\SaveData.txt";
        
        private String playerName;
        private int playerScore;
        private int playerTime;
        private int playerDifficulty;

        public Stats(GUIAdapter theAdapter)
        {
            adapter = theAdapter;
        }

        public void saveData()
        {
            String newLine = adapter.getPlayerName() + " " + adapter.getPlayerScore().ToString() + " " + adapter.getPlayerTime();
            String[] oldLines = File.ReadAllLines(path);
            List<String> lines = new List<String>();
            lines.Add(newLine);

            foreach (string r in oldLines)

            {
                lines.Add(r);
            }

            File.WriteAllLines(path, lines);
        }

        public void SaveJsonData()
        {

            playerName = adapter.getPlayerName();
            playerScore = adapter.getPlayerScore();
            playerTime = adapter.getPlayerTime();
            playerDifficulty = adapter.getBoardSize();

            List<T> data = new List<T>();

            List<T> oldData = Top5Saves();

            data.AddRange(oldData);

            data.Add(new T
            {
                Name = playerName,
                Score = playerScore,
                Time = playerTime,
                Difficulty = playerDifficulty
            }) ;

            string json = JsonConvert.SerializeObject(data.ToArray());

            File.WriteAllText(path, json);

        }

        public List<T> ReadJsonData()
        {
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                List<T> unpacked = JsonConvert.DeserializeObject<List<T>>(json);
                return unpacked;
            }
        }

        public List<T> Top5Saves()
        {
            List<T> sorted = new List<T>();

            //uses LINQ
            sorted = ReadJsonData().OrderBy(o => o.Score).ToList();

            int initialCount = sorted.Count;
            int currentCount;

            if (initialCount > 5)
            {
                for (int i = 0; i < initialCount; i++)
                {
                    currentCount = sorted.Count();
                    if (currentCount > 5)
                    {
                        sorted.RemoveAt(0);
                    }
                }
            }
            return sorted;
        }
    }
}
